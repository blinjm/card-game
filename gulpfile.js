// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var browserSync = require('browser-sync').create();
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cleanCSS = require('gulp-clean-css');
var gih = require("gulp-include-html");

// Include HTML
gulp.task('build-html' , function(){
    return gulp.src("./MK_content/source/*.html")
        .pipe(gih({
            baseDir:'./MK_content/'
        }))
        .pipe(gulp.dest("./MK_content"));
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('dist'))
        .pipe(cleanCSS())
        .pipe(rename('all.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

// create a task that ensures the `js` task is complete before
// reloading browsers
gulp.task('js-watch', ['scripts'], function (done) {
    browserSync.reload();
    done();
});
// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['js/vendor/*.js', 'js/*.js','node_modules/jquery-touchswipe/jquery.touchSwipe.min.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {

    //browserSync for HTML
    browserSync.init({
      files: ['*.htm', '*.html'],
      server: {
        baseDir: "./"
      },
      options: {
        reloadDelay: 250
      },
      notify: false
    });

    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('scss/_module/*.scss', ['sass']);
    gulp.watch('scss/_common/*.scss', ['sass']);
    gulp.watch('scss//*.scss', ['sass']);
    gulp.watch('MK_content/include/*.html', ['build-html']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
