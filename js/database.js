var db = window.openDatabase(
  'CardGame',           // dbName
  '1.0',            // version
  'Global database for the game',  // description
  2 * 1024 * 1024,  // estimatedSize in bytes
  function(db) {
    console.log('BDD created');
  }   // optional creationCallback
);


function initialisationBase() {
  db.transaction(function(tx) {
    // DELETE TESTS
    // tx.executeSql('DELETE FROM ecole WHERE ID=1');
    // tx.executeSql('DELETE FROM ecole WHERE ID=2');
    // tx.executeSql('DELETE FROM prof');
    //tx.executeSql('DROP TABLE IF EXISTS `prof`');

    // CREATION TABLE ECOLE
    tx.executeSql('CREATE TABLE IF NOT EXISTS `ecole` (' +
      '`ID` int(11), ' +
      '`Nom` text NOT NULL, ' +
      '`Argent` int(11) NOT NULL,' +
      '`Mana` int(11) NOT NULL,' +
      '`Reputation` int(11) NOT NULL,' +
      '`Num_tour` int(11) NOT NULL, ' +
      'PRIMARY KEY (`ID`) ' +
    ')');

    function initEcole(tx, results) {
      //console.log(results);
      var len = results.rows.length;
      var savedgame = false;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        if(row.ID == 1){
          savedgame    = true;
          tour        = parseInt(row.Num_tour);
          argent      = parseInt(row.Argent);
          mana        = parseInt(row.Mana);
          reputation  = parseInt(row.Reputation);
          nom         = row.Nom;
        }
      }
      if(!savedgame){
        tx.executeSql('INSERT INTO ecole(ID, Nom, Argent, Mana, Reputation, Num_tour) VALUES (?,?,?,?,?,?)', [1, 'Nom test', 100, 100, 1, 1]);
      }
    }

    tx.executeSql('SELECT * FROM ecole', [], initEcole, onError);

    // CREATION TABLE ELEVE
    tx.executeSql('CREATE TABLE IF NOT EXISTS `eleve` (' +
      '`ID_eleve` int(11) NOT NULL,' +
      '`nom` text NOT NULL,' +
      '`classe_ID` int(11) NOT NULL,' +
      '`age` int(11) NOT NULL,' +
      '`date_entree` int(11) NOT NULL,' +
      '`fav_mat_1` int(11) NOT NULL,' +
      '`fav_mat_2` int(11) NOT NULL,' +
      '`det_mat_1` int(11) NOT NULL,' +
      '`det_mat_2` int(11) NOT NULL,' +
      '`education` int(11) NOT NULL,' +
      '`magie_lumiere1` int(11) NOT NULL,' +
      '`magie_lumiere2` int(11) NOT NULL,' +
      '`magie_feu1` int(11) NOT NULL,' +
      '`magie_feu2` int(11) NOT NULL,' +
      '`magie_eau1` int(11) NOT NULL,' +
      '`magie_eau2` int(11) NOT NULL,' +
      '`magie_terre1` int(11) NOT NULL,' +
      '`magie_terre2` int(11) NOT NULL,' +
      '`magie_vent1` int(11) NOT NULL,' +
      '`magie_vent2` int(11) NOT NULL,' +
      '`magie_tenebre1` int(11) NOT NULL,' +
      '`magie_tenebre2` int(11) NOT NULL,' +
      '`magie_illusion1` int(11) NOT NULL,' +
      '`magie_illusion2` int(11) NOT NULL,' +
      '`magie_psy1` int(11) NOT NULL,' +
      '`magie_psy2` int(11) NOT NULL,' +
      '`mana_canalisation` int(11) NOT NULL,' +
      '`mana_projection` int(11) NOT NULL,' +
      '`mana_renforcement` int(11) NOT NULL,' +
      '`mana_manipulation` int(11) NOT NULL,' +
      '`savoir_histoire1` int(11) NOT NULL,' +
      '`savoir_histoire2` int(11) NOT NULL,' +
      '`savoir_mythe1` int(11) NOT NULL,' +
      '`savoir_mythe2` int(11) NOT NULL,' +
      '`savoir_monstre1` int(11) NOT NULL,' +
      '`savoir_monstre2` int(11) NOT NULL,' +
      '`savoir_plante1` int(11) NOT NULL,' +
      '`savoir_plante2` int(11) NOT NULL,' +
      '`prat_invocation1` int(11) NOT NULL,' +
      '`prat_invocation2` int(11) NOT NULL,' +
      '`prat_rune1` int(11) NOT NULL,' +
      '`prat_rune2` int(11) NOT NULL,' +
      '`prat_potion1` int(11) NOT NULL,' +
      '`prat_potion2` int(11) NOT NULL,' +
      '`prat_parchemin1` int(11) NOT NULL,' +
      '`prat_parchemin2` int(11) NOT NULL,' +
      'PRIMARY KEY (`ID_eleve`)' +
    ') ');
    // CREATION TABLE CLASSE
    tx.executeSql('CREATE TABLE IF NOT EXISTS `classe` (' +
      '`ID_classe` int(11) NOT NULL,' +
      '`tour1` int(11) NOT NULL,' +
      '`tour2` int(11) NOT NULL,' +
      '`tour3` int(11) NOT NULL,' +
      '`tour4` int(11) NOT NULL,' +
      '`tour5` int(11) NOT NULL,' +
      '`tour6` int(11) NOT NULL,' +
      '`tour8` int(11) NOT NULL,' +
      '`tour9` int(11) NOT NULL,' +
      '`tour10` int(11) NOT NULL,' +
      '`tour11` int(11) NOT NULL,' +
      '`tour13` int(11) NOT NULL,' +
      '`tour14` int(11) NOT NULL,' +
      '`tour15` int(11) NOT NULL,' +
      '`tour16` int(11) NOT NULL,' +
      '`magie_1` text NOT NULL,' +
      '`magie_2` text NOT NULL,' +
      '`magie_3` text NOT NULL,' +
      '`annee` int(11) NOT NULL,' +
      '`date_creation` int(11) NOT NULL,' +
      'PRIMARY KEY (`ID_classe`)' +
    ') ');
    // CREATION TABLE PROF
    tx.executeSql('CREATE TABLE IF NOT EXISTS `prof` (' +
      '`ID_prof` int(11) NOT NULL,' +
      '`pedagogie` int(11) NOT NULL,' +
      '`cycle` int(11) NOT NULL,' +
      '`paie` int(11) NOT NULL,' +
      '`mat1` text NOT NULL,' +
      '`mat2` text NOT NULL,' +
      '`mat3` text NOT NULL,' +
      '`mat4` text NOT NULL,' +
      '`niveau_mat1` int(11) NOT NULL,' +
      '`niveau_mat2` int(11) NOT NULL,' +
      '`niveau_mat3` int(11) NOT NULL,' +
      '`niveau_mat4` int(11) NOT NULL,' +
      '`experience` int(11) NOT NULL,' +
      '`age` int(11) NOT NULL,' +
      '`nom` text NOT NULL,' +
      '`rigueur` int(11) NOT NULL,' +
      '`accepte` int(11) NOT NULL,' +
      'PRIMARY KEY (`ID_prof`)' +
    ') ');
    // CREATION TABLE EXAMEN
    tx.executeSql('CREATE TABLE IF NOT EXISTS `examen` (' +
      '`ID_examen` int(11) NOT NULL,' +
      '`eleve_ID` int(11) NOT NULL,' +
      '`annee` int(11) NOT NULL,' +
      '`date` int(11) NOT NULL,' +
      '`resultat` varchar(5) NOT NULL,' +
      '`score` int(11) NOT NULL' +
    ') ');
    // CREATION TABLE MISSION
    tx.executeSql('CREATE TABLE IF NOT EXISTS `mission` (' +
      '`ID_mission` int(11) NOT NULL,' +
      '`eleve_ID` int(11) NOT NULL,' +
      '`annee` int(11) NOT NULL,' +
      '`date` int(11) NOT NULL,' +
      '`resultat` varchar(5) NOT NULL,' +
      '`score` int(11) NOT NULL' +
    ') ');
    // CREATION TABLE BATIMENTS
    tx.executeSql('CREATE TABLE IF NOT EXISTS `batiment` (' +
      '`ID_bat` int(11) NOT NULL,' +
      '`nom` text NOT NULL,' +
      '`domaine` text NOT NULL,' +
      '`buff` int(11) NOT NULL,' +
      '`nom_buff` text NOT NULL,' +
      'PRIMARY KEY (`ID_bat`)' +
    ')');


  }, opt_errorCallback, opt_successCallback);
}

// -- GESTION DES ERREURS
function opt_errorCallback(error){
  console.log(error);
  debug_jmb('ERROR : '+error, 'red');
}
function opt_successCallback(){
  debug_jmb('SUCCESS', 'green');
}
function onError(error) {
  console.log(error);
  debug_jmb('ERROR SQL : '+error, 'red');
}
