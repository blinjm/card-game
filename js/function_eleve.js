// AJOUT D'UN NOUVEL ELEVE
function newEleve(spe, cycle){

  db.transaction(function(tx) {

    tx.executeSql('SELECT * FROM eleve ORDER BY ID_prof DESC LIMIT 1', [], addProf, onError);

    function addProf(tx, results) {

      var len = results.rows.length;
      var lastID = 0;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        lastID = parseInt(row.ID_prof);
      }
      lastID++;
      
    }

  }, opt_errorCallback, opt_successCallback);

}
