function bindmenu(){

  $('.bt-menuProf').off().on('click', function(){
    if($(this).hasClass('selected')){
      $(this).removeClass('selected');
      $('.menu .bt').removeClass('hidden');
      $('.wrappercontent').html('');
    } else {
      $(this).addClass('selected');
      $('.menu .bt').not($(this)).addClass('hidden');
      gotopage('prof.html', 'prof');
    }
  });

  $('.bt-menuClasse').off().on('click', function(){
    if($(this).hasClass('selected')){
      $(this).removeClass('selected');
      $('.menu .bt').removeClass('hidden');
      $('.wrappercontent').html('');
    } else {
      $(this).addClass('selected');
      $('.menu .bt').not($(this)).addClass('hidden');
      gotopage('classe.html', 'classe');
    }
  });

}
