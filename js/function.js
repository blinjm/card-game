/** MAKHEIA **/
if (window.console) {
  var style;
  style = "background: #4daf26; color: #FFF; padding: 3px 5px; border-left: 5px solid #378718; margin-bottom: 3px;";
  var message = "This module has been developped by JMBLIN.";
  console.log('%c'+message, style);
}

/** DEBUG **/
function debug_jmb(text, color){
  var style;
  style = "background: "+color+"; color: #FFF; padding: 3px 5px; margin-bottom: 3px;";
  var message = text;
  console.log('%c'+message, style);
}

function gotopage(url_page, body_class, inmain){

  $('.transition').addClass('fade');
  setTimeout(function () {
    var target = $( ".wrappercontent" );
    if(inmain != undefined && inmain != "" && inmain != null) {
      target = $(''+inmain);
    }
    target.load( url_page, function() {
      if(body_class != undefined && body_class != "")
      $('body').attr('class', '').addClass(body_class);

      traduire();

      if(url_page == 'prof.html'){
        prof();
      }


      setTimeout(function () {
        $('.transition').addClass('fade_trans');

        setTimeout(function () {
          $('.transition').removeClass('fade fade_trans');
        }, 500);
      }, 501);

    });
  }, 500);
}
