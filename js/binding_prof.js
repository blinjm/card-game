var prof_choiceSpe = '';

function prof(){

  // NEW PROF
  $('.btn-newprof').off().on('click', function(){
    $('.listeprofs').addClass('hidden');
    $('.choiceSpe').removeClass('hidden');
    $('.choiceSpe, .btn-validChoiceSpe').show();
  });

  // CHOIX D'UNE SPECIALISATION
  $('.btn-validChoiceSpe').off().on('click', function(){
    prof_choiceSpe = $('.selectSpe').val();
    $('.choiceSpe').fadeOut(500);
    $('.btn-validChoiceSpe').fadeOut(500, function(){
      newProf(prof_choiceSpe, null, function(results){
        // console.log(results);
        for(var item in results){
          if($('.choice1 .'+item).length > 0){
            if($('.choice1 .'+item).attr('data-atrad') == 'true'){
              var temptrad = tradString(results[item]);
              $('.choice1 .'+item).html(temptrad);
            } else {
              $('.choice1 .'+item).html(results[item]);
            }
          }
        }
        $('.choice1').fadeIn(200);

        newProf(prof_choiceSpe, null, function(results2){
          //console.log(results);
          for(var item in results2){
            if($('.choice2 .'+item).length > 0){
              if($('.choice2 .'+item).attr('data-atrad') == 'true'){
                var temptrad = tradString(results2[item]);
                $('.choice2 .'+item).html(temptrad);
              } else {
                $('.choice2 .'+item).html(results2[item]);
              }
            }
          }
          $('.choice2').fadeIn(200);
        });
      });
    });
  });


  // CHOIX D'UN DES DEUX PROFS
  $('.btn-listeProfs').off().on('click', function(){
    $('.choiceSpe').addClass('hidden');
    generateListeProf();
    $('.listeprofs').removeClass('hidden');
  });

  // CHOIX D'UN DES DEUX PROFS
  $('.btn-choisir').off().on('click', function(){
    var monid_prof_accepte = $(this).parent().find('.ID_prof').html();
    //console.log(monid_prof_accepte);
    acceptProf(monid_prof_accepte);
    $(this).parent().addClass('selected');

    var monid_prof = $('.cardProf').not('.selected').find('.ID_prof').html();
    deleteProf(monid_prof);

    $('.cardProf').not('.selected').fadeOut(200, function(){
      $('.cardProf.selected').fadeOut(500, function(){
        generateListeProf();
        $('.listeprofs').removeClass('hidden');
      });
    });
  });

  //generateListeProf();
  function generateListeProf(){
    $('.nomsprofs').html('');
    allProfID(function(results){
      for(var item in results){
        myProf(results[item], function(res){
          // console.log(res);
          $('.nomsprofs').append('<div class="profprofile mat-'+res.mat1+'">'+'<div class="profilnom">'+res.nom+'</div>'+'<div class="profilpedagogie">'+res.pedagogie+'</div>'+'<div class="profilpaie">'+res.paie+'</div>'+'<div class="profilcycle">'+res.cycle+'</div>'+'<div class="profilage">'+res.age+'</div>'+'<div class="profilrigueur">'+res.rigueur+'</div>'+'</div>');
        });
      }
    });
  }

  // FILTER SELECT SPE
  $('.selectSpeList').on('change', function(){
    var mySelectedSpe = $(this).val();
    $('.profprofile').hide();
    $('.profprofile.mat-'+mySelectedSpe+'1, .profprofile.mat-'+mySelectedSpe+'2').show();
  });
  
}
