// MATIERE ALEATOIRE PAR CATEGORIE
function getRandomMat(categorie){

  var mat = '';
  var array_mat = [];
  if(categorie == 'Pratique'){
    array_mat = ['invocation', 'rune', 'potion', 'parchemin'];
  } else if(categorie == 'Savoir'){
    array_mat = ['histoire', 'mythe', 'monstre', 'plante'];
  } else if(categorie == 'Magie'){
    array_mat = ['feu', 'eau', 'terre', 'vent', 'lumiere', 'ombre', 'illusion', 'psy'];
  }
  var index = Math.ceil(Math.random() * array_mat.length) - 1;
  mat = array_mat[index];

  return mat;
}
