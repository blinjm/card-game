// AJOUT D'UN NOUVEAU PROF
function newProf(spe, cycle, callback){

  db.transaction(function(tx) {

    tx.executeSql('SELECT * FROM prof ORDER BY ID_prof DESC LIMIT 1', [], addProf, onError);

    function addProf(tx, results) {

      var len = results.rows.length;
      var lastID = -1;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        lastID = parseInt(row.ID_prof);
      }
      lastID++;

      var pedagogie = Math.ceil( Math.random() * 5 );
      var randomisation = Math.round( Math.random() * 1 ) + 1;
      var mat1 = spe+'1';
      var mat2 = spe+'2';
      if(randomisation == 1){
        mat1 = spe+'2';
        mat2 = spe+'1';
      }
      var mat3 = '';
      var mat4 = '';

      var array_magie = ['feu', 'eau', 'terre', 'vent', 'lumiere', 'ombre', 'illusion', 'psy'];
      var array_savoir = ['histoire', 'mythe', 'monstre', 'plante'];
      var array_pratique = ['invocation', 'rune', 'potion', 'parchemin'];

      if (array_magie.indexOf(spe) > -1) {
        mat3 = getRandomMat('Savoir');
        mat4 = getRandomMat('Pratique');
      } else if (array_savoir.indexOf(spe) > -1) {
        mat3 = getRandomMat('Magie');
        mat4 = getRandomMat('Pratique');
      } else if (array_pratique.indexOf(spe) > -1) {
        mat3 = getRandomMat('Savoir');
        mat4 = getRandomMat('Magie');
      }
      randomisation = Math.round( Math.random() * 1 ) + 1;
      mat3 += ''+randomisation;
      randomisation = Math.round( Math.random() * 1 ) + 1;
      mat4 += ''+randomisation;

      var niveau_mat1 = 3, niveau_mat2 = 2, niveau_mat3 = 1, niveau_mat4 = 1;
      var paie = 5;

      if(cycle == undefined || cycle == null || cycle == '' || cycle == 0) {
        cycle = Math.ceil( Math.random() * 3);
      }
      if(cycle == 3){
        niveau_mat1 = 5;
        niveau_mat2 = Math.ceil( Math.random() * 2) + 2;
        niveau_mat3 = Math.round( Math.random() * 2) + 1;
        niveau_mat4 = Math.round( Math.random() * 2) + 1;
        paie = 15;
      } else if(cycle == 2){
        niveau_mat1 = Math.round( Math.random() * 1) + 4;
        niveau_mat2 = Math.round( Math.random() * 2) + 2;
        niveau_mat3 = Math.round( Math.random() * 1) + 1;
        niveau_mat4 = Math.round( Math.random() * 1) + 1;
        paie = 10;
      } else {
        niveau_mat1 = Math.round( Math.random() * 2) + 3;
        niveau_mat2 = Math.round( Math.random() * 1) + 1;
      }

      var age = Math.round( Math.random() * 55) + 15;
      var nom = generateurNom('magie', spe);
      var rigueur = Math.ceil( Math.random() * 3);

      //console.log([lastID, pedagogie, cycle, paie, mat1, mat2, mat3, mat4, niveau_mat1, niveau_mat2, niveau_mat3, niveau_mat4, 0, age, nom, rigueur]);
      tx.executeSql('INSERT INTO prof(ID_prof, pedagogie, cycle, paie, mat1, mat2, mat3, mat4, niveau_mat1, niveau_mat2, niveau_mat3, niveau_mat4, experience, age, nom, rigueur, accepte) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [lastID, pedagogie, cycle, paie, mat1, mat2, mat3, mat4, niveau_mat1, niveau_mat2, niveau_mat3, niveau_mat4, 0, age, nom, rigueur, 0], feedbackID);

      function feedbackID(tx, resultats){
        //console.log(lastID);
        if(!isNaN(lastID)){
          myProf(lastID, function(montempresult){
            callback(montempresult);
          });
        }
      }
    }

  }, opt_errorCallback, opt_successCallback);


}

// ******************************************************************
// ASSIGNATION PROF A UNE CLASSE
// ******************************************************************
function assignProfClasse(idprofesseur, idclasse, tour){
  // On a le droit d'écraser un prof existant
  db.transaction(function(tx) {
    tx.executeSql('UPDATE classe SET tour'+tour+'='+idprofesseur+' WHERE ID_classe = '+idclasse+'', []);
  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// LISTE PROF DISPO
// ******************************************************************
function listeProfDispo(tour, callback){
  var array_prof = [];
  var final_proflibre = [];
  var tousmesprofs = [];

  db.transaction(function(tx) {
    tx.executeSql('SELECT prof.ID_prof, classe.tour'+tour+' FROM classe INNER JOIN prof on classe.tour'+tour+'=prof.ID_prof', [], getProf, onError);

    function getProf(tx, results) {
      var len = results.rows.length;
      var lastID = 0;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        lastID = parseInt(row.ID_prof);
        array_prof.push(lastID);
      }

      allProfID(function(result){
        var tousmesprofs = result;
        for(i=0;i<tousmesprofs.length;i++){
          if (array_prof.indexOf(tousmesprofs[i]) === -1) {
            final_proflibre.push(tousmesprofs[i]);
          }
        }
        callback(final_proflibre);
      });
    }

  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// LISTE DE TOUS LES PROFS
// ******************************************************************
function allProfID(callback){
  var array_prof = [];
  db.transaction(function(tx) {
    tx.executeSql('SELECT * FROM prof ORDER BY ID_prof', [], getAllProf, onError);
    function getAllProf(tx, results) {
      var len = results.rows.length;
      var lastID = 0;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        lastID = parseInt(row.ID_prof);
        array_prof.push(lastID);
      }
      callback(array_prof);
    }
  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// RECUPERATION D UN PROF PAR SON ID
// ******************************************************************
function myProf(id, callback){
  var myprof;
  db.transaction(function(tx) {
    tx.executeSql('SELECT * FROM prof WHERE ID_prof='+id, [], getAllProf, onError);
    function getAllProf(tx, results) {
      var len = results.rows.length;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        myprof = row;
      }
      callback(myprof);
    }
  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// ACCEPTER PROF
// ******************************************************************
function acceptProf(id){
  db.transaction(function(tx) {
    tx.executeSql('UPDATE prof SET accepte=1 WHERE ID_prof = '+id+'', []);
  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// SUPPRIMER PROF
// ******************************************************************
function deleteProf(id){
  var myprof;
  db.transaction(function(tx) {
    tx.executeSql('DELETE FROM prof WHERE ID_prof='+id, []);
  }, opt_errorCallback, opt_successCallback);
}

// ******************************************************************
// ALL PROF PAS ACCEPTE
// ******************************************************************
function deleteAllProfNA(){
  db.transaction(function(tx) {
    tx.executeSql('SELECT * FROM prof WHERE accepte=0', [], dAllProf, onError);
    function dAllProf(tx, results) {
      var len = results.rows.length;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        //console.log(row.ID_prof);
        deleteProf(row.ID_prof);
      }
    }
  }, opt_errorCallback, opt_successCallback);
}
