var traduction;

function trad(lang) {
  $.getJSON( "../languages/"+lang+".json", function( data ) {
    traduction = data;
    traduire();
  });
}

function traduire(){
  $('[data-trad]').each(function(){
    var temp_trad = $(this).data('trad');
    var temp_trad2 = traduction[temp_trad];
    //debug_mak(temp_trad, 'red');
    if(temp_trad2 != undefined && temp_trad2 != null && temp_trad2 != ""){
      temp_trad2 = temp_trad2.replace(/\(/g, "&#40;");
      temp_trad2 = temp_trad2.replace(/\)/g, "&#41;");
      temp_trad2 = temp_trad2.replace(/\s\?/g, "&nbsp;?");
      temp_trad2 = temp_trad2.replace(/\s\!/g, "&nbsp;!");
      temp_trad2 = temp_trad2.replace(/\s\:/g, "&nbsp;:");
      $(this).html(temp_trad2);
    } else {
      debug_mak(temp_trad, 'red');
    }
  });

  // debug_mak(traduction, 'green');
  // for (var key in traduction) {
  //   var mytrad = traduction[key];
  //   mytrad = mytrad.replace(/<br \/>/g, "__");
  //   mytrad = mytrad.replace(/<br\/>/g, "__");
  //   $('#benchmark').append(''+mytrad+'<br />');
  // }
}

function convertString(thestring){
  var mystring = thestring;
  if(mystring != undefined && mystring != null && mystring != ""){
    mystring = mystring.replace(/\(/g, "&#40;");
    mystring = mystring.replace(/\)/g, "&#41;");
    mystring = mystring.replace(/\s\?/g, "&nbsp;?");
    mystring = mystring.replace(/\s\!/g, "&nbsp;!");
    mystring = mystring.replace(/\s\:/g, "&nbsp;:");
    mystring = mystring.replace(/\s\:/g, "&nbsp;:");
    mystring = mystring.replace(/Van Cleef & Arpels/g, "Van&nbsp;Cleef&nbsp;&amp;&nbsp;Arpels");
  }
  return mystring;
}

function tradString(id){
  //console.log(id);
  var mystring = "";
  switch (id) {
    case 'feu1':
      mystring = "FEU - Pyromancie";
      break;
    case 'feu2':
      mystring = "FEU - Calorimancie";
      break;
    case 'eau1':
      mystring = "EAU - Glaciologie";
      break;
    case 'eau2':
      mystring = "EAU - Aquamancie";
      break;
    case 'terre1':
      mystring = "TERRE - Magie du métal";
      break;
    case 'terre2':
      mystring = "TERRE - Druidisme";
      break;
    case 'vent1':
      mystring = "VENT - Ariamancie";
      break;
    case 'vent2':
      mystring = "VENT - Magie des rêves";
      break;
    case 'lumiere1':
      mystring = "LUMIERE - Magie de purification";
      break;
    case 'lumiere2':
      mystring = "LUMIERE - Magie de guérison";
      break;
    case 'ombre1':
      mystring = "TENEBRES - Nécromancie";
      break;
    case 'ombre2':
      mystring = "TENEBRES - Magie des ombres";
      break;
    case 'illusion1':
      mystring = "ILLUSION - Illusionnisme";
      break;
    case 'illusion2':
      mystring = "ILLUSION - Magie des portails";
      break;
    case 'psy1':
      mystring = "PSYCHISME - Divination";
      break;
    case 'psy2':
      mystring = "PSYCHISME - Espéromancie";
      break;
    case 'histoire1':
      mystring = "HISTOIRE - Discours";
      break;
    case 'histoire2':
      mystring = "HISTOIRE - Archivage";
      break;
    case 'mythe1':
      mystring = "MYTHE - Exploration";
      break;
    case 'mythe2':
      mystring = "MYTHE - Archéologie";
      break;
    case 'monstre1':
      mystring = "MONSTRE - Zoologie";
      break;
    case 'monstre2':
      mystring = "MONSTRE - Dresseur";
      break;
    case 'plante1':
      mystring = "PLANTE - Botanique";
      break;
    case 'plante2':
      mystring = "PLANTE - Cuisine";
      break;
    case 'invocation1':
      mystring = "INVOCATION - Appel";
      break;
    case 'invocation2':
      mystring = "INVOCATION - Bannissement";
      break;
    case 'rune1':
      mystring = "Enchantements";
      break;
    case 'rune2':
      mystring = "Gravure de rune";
      break;
    case 'potion1':
      mystring = "POTION - Alchimie";
      break;
    case 'potion2':
      mystring = "POTION - Médecine";
      break;
    case 'parchemin1':
      mystring = "PARCHEMIN - Calligraphie";
      break;
    case 'parchemin2':
      mystring = "PARCHEMIN - Administration";
      break;
    default:
      mystring = "";
  }

  return mystring;
}
