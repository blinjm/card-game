function generateurNom(categorie, spe){
  var nomtemp = '';
  var array_nomM = [];
  var array_nomF = [];
  var sexeNom = Math.round(Math.random()); // 0 = homme // 1 = femme
  var array_nommage = [];
  var index = 0;

  if(categorie != undefined && categorie != null && categorie != ''){
    array_nomM = ['Arcturus', 'Bernod', 'Codric', 'Demmor', 'Emeth', 'Fédrik', 'Garrisson', 'Héristophane', 'Idriss', 'Jon', 'Kendoss', 'Lordin', 'Mérimé', 'Nurge', 'Odéron', 'Pardan', 'Rupian', 'Soyos', 'Théodule', 'Uvô', 'Victorio', 'Zarian'];
    array_nomF = ['Aliénor', 'Britta', 'Cléomestra', 'Dalhia', 'Ellissandra', 'Fillipa', 'Gurdine', 'Héléna', 'Iphène', 'Jullianna', 'Kaya', 'Lune', 'Masako', 'Nostène', 'Octavia', 'Pollianna', 'Ramma', 'Saramia', 'Titia', 'Umélia', 'Visséa', 'Zia'];

    // -- MAGE --
    if(categorie == 'magie'){
      if(spe == 'feu'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Brûlant', 'Brasier', 'des Flammes éternelles', 'des Flammes divines', 'des Flammes bleues', 'le Volcan', 'la tempête de Flammes', 'l\'ardant'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Brûlante', 'Brasier', 'des Flammes éternelles', 'des Flammes divines', 'des Flammes bleues', 'le Volcan', 'la tempête de Flammes', 'l\'ardante'];
        }
      } else if(spe == 'eau'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['la Cascade', 'le Glacier', 'le Blizzard', 'le Givré', 'l\'homme des glaces', 'de la Pluie', 'de la Neige', 'le Tranquille', 'le Ruisseau', 'la Rivière'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Cascade', 'le Glacier', 'le Blizzard', 'la Givrée', 'la femme des glaces', 'de la Pluie', 'de la Neige', 'le Tranquille', 'le Ruisseau', 'la Rivière'];
        }
      } else if(spe == 'vent'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Ouragan', 'la Rafale', 'le Vent du Sud', 'la Bourrasque', 'la Brise', 'le Cyclone', 'le Tourbillonnant', 'le Rapide', 'le Flottant'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Ouragan', 'la Rafale', 'le Vent du Sud', 'la Bourrasque', 'la Brise', 'le Cyclone', 'la Tourbillonnante', 'la Rapide', 'la Flottante'];
        }
      } else if(spe == 'terre'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Roc', 'la Montagne', 'le Fleuri', 'de la forêt Noire', 'du bois du Nord', 'l\'Ermite', 'le Grand Saule', 'le Cueilleur', 'les Pouces Verts'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['le Roc', 'la Montagne', 'la Fleurie', 'de la forêt Noire', 'du bois du Nord', 'l\'Ermite', 'le Grand Saule', 'la Cueilleuse', 'les Pouces Verts'];
        }
      } else if(spe == 'lumiere'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Illuminé', 'l\'Etincelant', 'le Lumineux', 'le Purificateur', 'le Saint', 'le Divin', 'le Bon', 'le Généreux', 'le Protecteur'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Illuminée', 'l\'Etincelante', 'la Lumineuse', 'la Purificatrice', 'la Sainte', 'la Divine', 'la Souriante', 'la Généreuse', 'la Protectrice'];
        }
      } else if(spe == 'ombre'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Noir', 'le Sombre', 'le Terrifiant', 'le Discret', 'du Grand Tombeau', 'l\'Encapuchonné', 'le Sinistre', 'le Comploteur', 'des Marais'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Noire', 'la Sombre', 'la Terrifiante', 'la Discrète', 'du Grand Tombeau', 'l\'Encapuchonnée', 'la Sinistre', 'la Comploteuse', 'des Marais'];
        }
      } else if(spe == 'histoire'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Cultivé', 'le Sage', 'le Bienveillant', 'le Calme'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Cultivée', 'la Sage', 'la Bienveillante', 'la Calme'];
        }
      } else if(spe == 'mythe'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Légendaire', 'l\'Incroyable', 'le Mythique', 'l\'Extraordinaire', 'l\'Epatant'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Légendaire', 'l\'Incroyable', 'la Mythique', 'l\'Extraordinaire', 'l\'Epatante'];
        }
      } else if(spe == 'monstre'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Monstrueux', 'le Baveux', 'l\'Animal', 'le Féroce'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Monstrueuse', 'la Baveuse', 'l\'Animal', 'la Féroce'];
        }
      } else if(spe == 'plante'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['le Verdoyant', 'l\'Herboriste', 'le Fumeux', 'le Savant'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['la Verdoyante', 'l\'Herboriste', 'la Fumeuse', 'la Savante'];
        }
      } else if(spe == 'invocation'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Invocateur', 'le Bannisseur', 'le Maître des Portails', 'le Maître des Plans', 'du Vide'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Invocatrice', 'la Bannisseuse', 'la Maître des Portails', 'la Maître des Plans', 'du Vide'];
        }
      } else if(spe == 'rune'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Enchanteur', 'le Maître des Runes', 'le Graveur', 'le Forgeron de Mana', 'l\'Enlimineur'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Enchanteuse', 'la Maître des Runes', 'la Graveuse', 'le Forgeron de Mana', 'l\'Enlimineuse'];
        }
      } else if(spe == 'potion'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Alchimiste', 'le Transmutateur', 'le Félé', 'le Doré', 'du Laboratoire'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Alchimiste', 'la Transmutatrice', 'la Félée', 'la Dorée', 'du Laboratoire'];
        }
      } else if(spe == 'parchemin'){
        if(sexeNom == 0){
          index = Math.ceil(Math.random() * array_nomM.length) - 1;
          nomtemp = array_nomM[index];
          array_nommage = ['l\'Ecrivain', 'le Scribe', 'la Plume', 'le Pointilleux'];
        } else {
          index = Math.ceil(Math.random() * array_nomF.length) - 1;
          nomtemp = array_nomF[index];
          array_nommage = ['l\'Ecrivaine', 'la Scribe', 'la Plume', 'la Pointilleuse'];
        }
      }
      index = Math.ceil(Math.random() * array_nommage.length) - 1;
      nomtemp += ' '+array_nommage[index];
    } else {
      if(spe == 'noble'){
        array_nommage = ['de Rotambourg', 'de Volovant', 'de Margeton', 'de Grand-Champs', 'de la Ceraudou', 'de Rochebrunes', 'de Sensonain'];
      } else if(spe == 'bourgeois'){
        array_nommage = ['Mazarand', 'Turieu', 'Franagonde', 'Hissonde', 'Pellerin', 'Kraft', 'Elandare', 'Ferna', 'Mirmion', 'Banlieu'];
      } else if(spe == 'peuple'){
        array_nommage = ['Payson', 'Forgerain', 'Curetin', 'Aubregeon', 'Prospitute', 'Chassa', 'Bocheron', 'Fantasson', 'Cavalerand'];
      } else {
        array_nommage = ['Poissard', 'Mizaire', 'Petitbidon', 'Sans-dent'];
      }
      index = Math.ceil(Math.random() * array_nommage.length) - 1;
      nomtemp += ' '+array_nommage[index];
    }
  }

  return nomtemp;
}
