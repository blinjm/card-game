// AJOUT D'UNE NOUVELLE CLASSE
function newClasse(annee){

  db.transaction(function(tx) {

    tx.executeSql('SELECT * FROM classe ORDER BY ID_classe DESC LIMIT 1', [], addClasse, onError);

    function addClasse(tx, results) {
      var len = results.rows.length;
      var lastID = 0;
      for (var i = 0; i < len; ++i) {
        var row = results.rows.item(i);
        lastID = parseInt(row.ID_classe);
      }
      lastID++;

      tx.executeSql('INSERT INTO classe(ID_classe, tour1, tour2, tour3, tour4, tour5, tour6,  tour8, tour9, tour10, tour11, tour13, tour14, tour15, tour16, magie_1, magie_2, magie_3, annee, date_creation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [lastID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'','','',annee,tour]);
    }

  }, opt_errorCallback, opt_successCallback);

}

// *********************************
// ASSOCIER UNE CLASSE A UNE ANNEE
// *********************************
function anneeClasse(id, annee){
  db.transaction(function(tx) {
    tx.executeSql('UPDATE classe SET annee='+annee+' WHERE ID_classe = '+id+'', []);
  }, opt_errorCallback, opt_successCallback);
}
